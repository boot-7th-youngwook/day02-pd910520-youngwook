package com.posco.spring.board.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.spring.board.entity.Article;
import com.posco.spring.board.service.BoardService;

@RestController
@RequestMapping(value="/article")
public class ArticleController {
	
	@Autowired
	private BoardService boardService;
	
	@PostMapping("/addArticle")
	public void registerArtcle(@RequestBody Article article) {
		
		boardService.registerArticle(article);
	}
	
	@GetMapping(value="/select/{articleId}")
	public Article selectArticle(@PathVariable String articleId) {
		return boardService.findArticle(articleId);
	}
	
	
	@PostMapping("/updateArtcle")
	public void updateArticle(@RequestBody Article article) {
		boardService.modifyArticle(article);
	}
	
	@GetMapping(value="/delete/{articleId}")
	public void deleteArticle(@PathVariable String articleId) {
		boardService.removeArticle(articleId);
	}
}

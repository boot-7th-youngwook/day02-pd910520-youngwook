package com.posco.spring.board.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.spring.board.entity.Comment;
import com.posco.spring.board.service.BoardService;

@RestController
@RequestMapping(value="/comment")
public class CommentController {
	
	@Autowired
	private BoardService boardService;
	
	@PostMapping("/addArticle")
	public void registerComment(@RequestBody Comment comment) {
		
		boardService.registerComment(comment);
	}
	
	@GetMapping(value="/delete/{commentId}")
	public void deleteComment(@PathVariable String commentId) {
		boardService.removeComment(commentId);
	}
}

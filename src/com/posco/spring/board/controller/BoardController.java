package com.posco.spring.board.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.spring.board.entity.Board;
import com.posco.spring.board.service.BoardService;

@RestController
@RequestMapping(value="/board")
public class BoardController {

	@Autowired
	private BoardService boardService;
	
	@PostMapping("/addBoard")
	public String registerBoard(@RequestBody Board board) {
		System.out.println("registerBoard");
		return boardService.registerBoard(board);
	}
	
	@GetMapping(value= {"/",""})
	public List<Board> findAllBoards(){
		return boardService.findAllBoards();
	}
	
	
	@GetMapping(value= "/select/{boardId}")
	public Board findBoards(@PathVariable String boardId){
		return boardService.findBoard(boardId);
	}
	
	
	@PostMapping("/updateBoard")
	public void updateBoard(@RequestBody Board board) {
		boardService.modifyBoard(board);
	}
	
	@GetMapping(value="/delete/{boardId}")
	public void deleteBoard(@PathVariable String boardId) {
		boardService.removeBoard(boardId);
	}
	
}

package com.posco.spring.board.store.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.posco.spring.board.entity.Article;
import com.posco.spring.board.entity.Board;

public class ArticleRepository {

	
	private static ArticleRepository instance;
	
	private Map<String,Article> articleMap;
	
	private ArticleRepository() {
		this.articleMap = new HashMap<String,Article>();
	}
	
	public static ArticleRepository getInstance() {
		if(instance == null) {
			instance = new ArticleRepository();
		}
		return instance;
	}
	
	public String insert(Article article) {
		this.articleMap.put(article.getArticleId(),article);
		return article.getArticleId();
	}
	
	public List<Article> selectAll(){
		 
		return this.articleMap.values().stream()
									 .collect(Collectors.toList());
	}
	
	public Article selectById(String articleId) {
		return articleMap.values().stream()
								.filter(article -> article.getArticleId().equals(articleId))
								.findFirst().get();
//		return boardMap.get(boardId);
	}
	
	public void update(Article article) {
		this.articleMap.put(article.getArticleId(),article);
	}
	
	public void delete(String articleId) {
		Article foundArticle = articleMap.values().stream()
											.filter(article -> article.getArticleId().equals(articleId))
											.findFirst().get();
		
//		Board foundBoard = boardMap.get(boardId);
		if(foundArticle == null) {
			return;
		}
		articleMap.remove(articleId);
	}
	
}

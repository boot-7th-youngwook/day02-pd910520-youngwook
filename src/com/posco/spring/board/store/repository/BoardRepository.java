package com.posco.spring.board.store.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.posco.spring.board.entity.Board;

public class BoardRepository {

	
	private static BoardRepository instance;
	
	private Map<String,Board> boardMap;
	
	private BoardRepository() {
		this.boardMap = new HashMap<String,Board>();
	}
	
	public static BoardRepository getInstance() {
		if(instance == null) {
			instance = new BoardRepository();
		}
		return instance;
	}
	
	public String insert(Board board) {
		this.boardMap.put(board.getBoardId(),board);
		return board.getBoardId();
	}
	
	public List<Board> selectAll(){
		 
		return this.boardMap.values().stream()
									 .collect(Collectors.toList());
	}
	
	public Board selectById(String boardId) {
		return boardMap.values().stream()
								.filter(board -> board.getBoardId().equals(boardId))
								.findFirst().get();
//		return boardMap.get(boardId);
	}
	
	public void update(Board board) {
		this.boardMap.put(board.getBoardId(),board);
	}
	
	public void delete(String boardId) {
		Board foundBoard = boardMap.values().stream()
											.filter(board -> board.getBoardId().equals(boardId))
											.findFirst().get();
		
//		Board foundBoard = boardMap.get(boardId);
		if(foundBoard == null) {
			return;
		}
		boardMap.remove(boardId);
	}
	
}

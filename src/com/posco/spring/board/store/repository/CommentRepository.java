package com.posco.spring.board.store.repository;

import java.util.HashMap;
import java.util.Map;

import com.posco.spring.board.entity.Comment;


public class CommentRepository {

	
	private static CommentRepository instance;
	
	private Map<String,Comment> commentMap;
	
	private CommentRepository() {
		this.commentMap = new HashMap<String,Comment>();
	}
	
	public static CommentRepository getInstance() {
		if(instance == null) {
			instance = new CommentRepository();
		}
		return instance;
	}
	
	public String insert(Comment comment) {
		this.commentMap.put(comment.getCommentId(),comment);
		return comment.getCommentId();
	}
	
	
	public void delete(String commentId) {
		Comment foundComment = commentMap.values().stream()
											.filter(comment -> comment.getCommentId().equals(commentId))
											.findFirst().get();
		
//		Comment foundComment = commentMap.get(CommentId);
		if(foundComment == null) {
			return;
		}
		commentMap.remove(commentId);
	}

	public Comment retrieve(String commentId) {
		// TODO Auto-generated method stub
		 return commentMap.values().stream()
				.filter(comment -> comment.getCommentId().equals(commentId))
				.findFirst().get();
	}
	
}

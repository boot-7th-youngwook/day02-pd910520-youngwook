package com.posco.spring.board.store;

import java.util.List;

import com.posco.spring.board.entity.Comment;

public interface CommentStore {

	String create(Comment comment);
	List<Comment> retrieveAll(String articleId);
	
	Comment retrieve(String commentId);
	void delete(String commentId);
}

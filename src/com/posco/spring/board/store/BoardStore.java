package com.posco.spring.board.store;

import java.util.List;

import com.posco.spring.board.entity.Board;

public interface BoardStore {
	
	String create(Board board);
	Board retrieve(String boardId);
	List<Board> retrieveAll();
	
	void update(Board board);
	void delete(String boardId);

}

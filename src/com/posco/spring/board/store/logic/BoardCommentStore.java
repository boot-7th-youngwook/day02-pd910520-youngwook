package com.posco.spring.board.store.logic;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.posco.spring.board.entity.Comment;
import com.posco.spring.board.store.CommentStore;
import com.posco.spring.board.store.repository.CommentRepository;

@Repository
public class BoardCommentStore implements CommentStore {

	private CommentRepository commentRepo;
	
	public BoardCommentStore() {
		this.commentRepo = CommentRepository.getInstance();
	}

	@Override
	public String create(Comment comment) {
		// TODO Auto-generated method stub
		
		
		return commentRepo.insert(comment);
	}

	@Override
	public List<Comment> retrieveAll(String articleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String commentId) {
		// TODO Auto-generated method stub
		commentRepo.delete(commentId);
	}

	@Override
	public Comment retrieve(String commentId) {
		// TODO Auto-generated method stub
		return commentRepo.retrieve(commentId);
	}
	

}

package com.posco.spring.board.store.logic;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.posco.spring.board.entity.Board;
import com.posco.spring.board.store.BoardStore;
import com.posco.spring.board.store.repository.BoardRepository;

@Repository
public class BoardMemStore implements BoardStore {

	private BoardRepository boardRepo;
	
	public BoardMemStore() {
		this.boardRepo = BoardRepository.getInstance();
	}
	@Override
	public String create(Board board) {
		return this.boardRepo.insert(board);
	}

	@Override
	public Board retrieve(String boardId) {
		// TODO Auto-generated method stub
		return this.boardRepo.selectById(boardId);
	}

	@Override
	public List<Board> retrieveAll() {
		// TODO Auto-generated method stub
		return this.boardRepo.selectAll();
	}

	@Override
	public void update(Board board) {
		// TODO Auto-generated method stub
		this.boardRepo.update(board);
	}

	@Override
	public void delete(String boardId) {
		// TODO Auto-generated method stub
		this.boardRepo.delete(boardId);
	}

}

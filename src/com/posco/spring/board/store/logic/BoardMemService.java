package com.posco.spring.board.store.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.posco.spring.board.entity.Article;
import com.posco.spring.board.entity.Board;
import com.posco.spring.board.entity.Comment;
import com.posco.spring.board.service.BoardService;
import com.posco.spring.board.store.ArticleStore;
import com.posco.spring.board.store.BoardStore;
import com.posco.spring.board.store.CommentStore;

@Service
public class BoardMemService implements BoardService{

	@Autowired
	private BoardStore boardStore;
	
	@Autowired
	private ArticleStore articleStore;
	
	@Autowired
	private CommentStore commentStore;
	
	@Override
	public void registerArticle(Article article) {
		// TODO Auto-generated method stub
		Board board = boardStore.retrieve(article.getBoardId());
		articleStore.create(article);
		List<Article> articleList = articleStore.retrieveAll(article.getBoardId());
		if(articleList != null && articleList.size() > 0) {
			board.setArticles(articleList);
		}
		boardStore.create(board);
	}

	@Override
	public Article findArticle(String articleId) {
		// TODO Auto-generated method stub
		return articleStore.retrieve(articleId);
	}

	@Override
	public Board findBoard(String boardId) {
		// TODO Auto-generated method stub
		return boardStore.retrieve(boardId);
	}

	@Override
	public void removeArticle(String articleId) {
		// TODO Auto-generated method stub
		Article article = articleStore.retrieve(articleId);
		Board board = boardStore.retrieve(article.getBoardId());
		articleStore.delete(articleId);
		List<Article> articleList = articleStore.retrieveAll(article.getBoardId());
		if(articleList != null && articleList.size() > 0) {
			board.setArticles(articleList);
		}
		boardStore.create(board);
	}

	@Override
	public void modifyArticle(Article article) {
		// TODO Auto-generated method stub
		
		Board board = boardStore.retrieve(article.getBoardId());
		articleStore.update(article);
		List<Article> articleList = articleStore.retrieveAll(article.getBoardId());
		if(articleList != null && articleList.size() > 0) {
			board.setArticles(articleList);
		}
		boardStore.update(board);
	}

	@Override
	public String registerBoard(Board board) {
		// TODO Auto-generated method stub
		return boardStore.create(board);
		
	}

	@Override
	public List<Board> findAllBoards() {
		// TODO Auto-generated method stub
		return boardStore.retrieveAll();
	}

	@Override
	public void modifyBoard(Board board) {
		// TODO Auto-generated method stub
		 boardStore.update(board);
	}

	@Override
	public void removeBoard(String boardId) {
		// TODO Auto-generated method stub
		boardStore.delete(boardId);
	}

	@Override
	public void registerComment(Comment comment) {
		// TODO Auto-generated method stub
		Article article = articleStore.retrieve(comment.getArticleId());
		Board board = boardStore.retrieve(article.getBoardId());
		commentStore.create(comment);
		List<Comment> commentList = commentStore.retrieveAll(comment.getArticleId());
		if(commentList != null && commentList.size() > 0) {
			article.setComments(commentList);
		}
		List<Article> articleList = articleStore.retrieveAll(article.getBoardId());
		if(articleList != null && articleList.size() > 0) {
			board.setArticles(articleList);
		}
		boardStore.create(board);
	}

	@Override
	public void removeComment(String commentId) {
		// TODO Auto-generated method stub
		
		
		
		Comment comment = commentStore.retrieve(commentId);
		commentStore.delete(commentId);
		Article article = articleStore.retrieve(comment.getArticleId());
		
		List<Comment> commentsList = article.getComments();
		
		Board board = boardStore.retrieve(article.getBoardId());
		List<Article> articleList = articleStore.retrieveAll(article.getBoardId());
		if(articleList != null && articleList.size() > 0) {
			board.setArticles(articleList);
		}
		
		boardStore.create(board);
	}
	
}

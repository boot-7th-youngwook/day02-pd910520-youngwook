package com.posco.spring.board.store.logic;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.posco.spring.board.entity.Article;
import com.posco.spring.board.entity.Board;
import com.posco.spring.board.store.ArticleStore;
import com.posco.spring.board.store.repository.ArticleRepository;

@Repository
public class BoardArtStore implements ArticleStore {

	private ArticleRepository artRepo;
	
	public BoardArtStore() {
		this.artRepo = ArticleRepository.getInstance();
	}

	@Override
	public String create(Article article) {
		// TODO Auto-generated method stub
		return artRepo.insert(article);
	}

	@Override
	public Article retrieve(String articleId) {
		// TODO Auto-generated method stub
		return artRepo.selectById(articleId);
	}

	@Override
	public List<Article> retrieveAll(String boardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Article article) {
		// TODO Auto-generated method stub
		artRepo.update(article);
	}

	@Override
	public void delete(String articleId) {
		// TODO Auto-generated method stub
		artRepo.delete(articleId);
	}
	

}
